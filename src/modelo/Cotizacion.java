
package modelo;

public class Cotizacion {
    private String NumCoti;
    private String DescAuto;
    private float Precio;
    private float PorcPago;
    private int plazo;
    //Contructores
    //Vacio
    public Cotizacion(){
    }
    //Parametros
    public Cotizacion(String NumCoti,String DescAuto,float Precio,float PorcPago,int plazo){
        this.NumCoti = NumCoti;
        this.DescAuto = DescAuto;
        this.Precio = Precio;
        this.PorcPago = PorcPago;
        this.plazo = plazo;
    }
    //Copia
    public Cotizacion(Cotizacion Cotz){
        this.NumCoti = Cotz.NumCoti;
        this.DescAuto = Cotz.DescAuto;
        this.Precio = Cotz.Precio;
        this.PorcPago = Cotz.PorcPago;
        this.plazo = Cotz.plazo;
    }
    //Metodos Set & Get

    public String getNumCoti() {
        return NumCoti;
    }

    public void setNumCoti(String NumCoti) {
        this.NumCoti = NumCoti;
    }

    public String getDescAuto() {
        return DescAuto;
    }

    public void setDescAuto(String DescAuto) {
        this.DescAuto = DescAuto;
    }

    public float getPrecio() {
        return Precio;
    }

    public void setPrecio(float Precio) {
        this.Precio = Precio;
    }

    public float getPorcPago() {
        return PorcPago;
    }

    public void setPorcPago(float PorcPago) {
        this.PorcPago = PorcPago;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    //metodos para la muestra de la Cotizacion
    public float CalculoPagoInicial(){
        return this.Precio*(this.PorcPago / 100);
    }
    public float CalculoTotalFinanciero(){
        return this.Precio - CalculoPagoInicial();
    }
    public float CalculoPagoMensual(){
        return CalculoTotalFinanciero() / getPlazo();
    }
}
