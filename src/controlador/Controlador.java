
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Cotizacion;
import vista.dlgCotizacion;

public class Controlador implements ActionListener {
    private Cotizacion coti;
    private dlgCotizacion vista;

    public Controlador(Cotizacion coti, dlgCotizacion vista) {
        this.coti = coti;
        this.vista = vista;
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnMostrar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
    }
    
    private void iniciarVista(){
        vista.setTitle(".:: Concesionaria Toyota Mazatlan ::.");
        //Esta linea de codigo es para modifica el (Width,Heigth)
        vista.setSize(685,328);//<<<<----^^^^^^^^^^^^^^^^^^^^^^^^
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==vista.btnLimpiar){
            vista.txtNumCoti.setText("");
            vista.txtDescAuto.setText("");
            vista.txtPrecio.setText("");
            vista.txtPorcPago.setText("");
            vista.cbPlazo.setEditable(false);
            vista.txtPagoInicial.setText("");
            vista.txtTotalFinanciero.setText("");
            vista.txtPagoMensual.setText("");
        }
        if(e.getSource()==vista.btnNuevo){
            vista.txtNumCoti.setEditable(true);
            vista.txtDescAuto.setEditable(true);
            vista.txtPrecio.setEditable(true);
            vista.txtPorcPago.setEditable(true);
            vista.cbPlazo.setEditable(true);
            vista.txtPagoInicial.setEditable(true);
            vista.txtTotalFinanciero.setEditable(true);
            vista.txtPagoMensual.setEditable(true);
        }
        if(e.getSource()==vista.btnGuardar){
            coti.setNumCoti(vista.txtNumCoti.getText());
            coti.setDescAuto(vista.txtDescAuto.getText());
            try{
                float x=(Float.parseFloat(vista.txtPorcPago.getText()));
                if(x<=100){
                    coti.setPorcPago(x);
                }
                else{
                    JOptionPane.showMessageDialog(vista,"El porcentaje no puede ser mayor a 100");
                    return;
                }
                coti.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                coti.setPlazo(Integer.parseInt(vista.cbPlazo.getSelectedItem().toString()));
                JOptionPane.showMessageDialog(vista,"Se agrego Exitosamente"); 
            }catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex2.getMessage());
            }
            vista.txtPagoMensual.setText(String.valueOf(coti.CalculoPagoMensual()));
            vista.txtTotalFinanciero.setText(String.valueOf(coti.CalculoTotalFinanciero()));
            vista.txtPagoInicial.setText(String.valueOf(coti.CalculoPagoInicial()));
        }
        
        if(e.getSource()==vista.btnMostrar){
            vista.txtDescAuto.setText(coti.getDescAuto());
            vista.txtNumCoti.setText(coti.getNumCoti());
            vista.txtPrecio.setText(String.valueOf(coti.getPrecio()));
            vista.txtPorcPago.setText(String.valueOf(coti.getPorcPago()));
            vista.cbPlazo.setSelectedItem(String.valueOf(coti.getPlazo()));
            vista.txtPagoMensual.setText(String.valueOf(coti.CalculoPagoMensual()));
            vista.txtTotalFinanciero.setText(String.valueOf(coti.CalculoTotalFinanciero()));
            vista.txtPagoInicial.setText(String.valueOf(coti.CalculoPagoInicial()));
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
        
        if(e.getSource()==vista.btnCancelar){
            vista.txtDescAuto.setText("");
            vista.txtNumCoti.setText("");
            vista.txtPagoInicial.setText("");
            vista.txtPagoMensual.setText("");
            vista.txtPorcPago.setText("");
            vista.txtPrecio.setText("");
            vista.txtTotalFinanciero.setText("");
            vista.txtNumCoti.setEnabled(false);
            vista.txtDescAuto.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.txtPorcPago.setEnabled(false);
            vista.btnMostrar.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.cbPlazo.setEnabled(false);
        }
    }
    
    public static void main(String[] args) {
        Cotizacion coti = new Cotizacion();
        dlgCotizacion vista = new dlgCotizacion(new JFrame(),true);
        
        Controlador control = new Controlador(coti,vista);
        control.iniciarVista();        
    }
}
